import subprocess


f1 = open("/etc/bind/named.conf.local", "w")
f2 = open("/mnt/Lab3/DNS-keeper/SettingsZone", "r")
lines = f2.readlines()
for line in lines:
    f1.write(line)
f1.close()
f2.close()

subprocess.call(["sudo", "cp", "/etc/bind/db.local", "/var/lib/bind/db.lab.loc"])

f1 = open("/var/lib/bind/db.lab.loc", "w")
f2 = open("/mnt/Lab3/DNS-keeper/db.local", "r")
lines = f2.readlines()
for line in lines:
    f1.write(line)
f1.close()
f2.close()

subprocess.call(["sudo", "cp", "/etc/bind/db.127", "/var/lib/bind/db.192"])

f1 = open("/var/lib/bind/db.192", "w")
f2 = open("/mnt/Lab3/DNS-keeper/db.192", "r")
lines = f2.readlines()
for line in lines:
    f1.write(line)
f1.close()
f2.close()

# subprocess.call(["sudo", "service", "bind9", "restart"])

