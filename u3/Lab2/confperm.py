import subprocess as su

f = open("/etc/network/interfaces", "w")
lines = ['auto lo', 'iface lo inet loopback', 'auto eth0', 
'iface eth0 inet dhcp']
for line in lines:
    f.write(line + '\n')
f.close()


su.call(['ifdown', 'eth0'])
su.call(['ifup', 'eth0'])
