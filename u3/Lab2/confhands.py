import subprocess as su

f = open("/etc/network/interfaces", "w")
f.write('auto lo\n')
f.write('iface lo inet loopback')
f.close()


su.call(['ifdown', 'eth0'])
su.call(['dhclient', 'eth0', '-v'])
