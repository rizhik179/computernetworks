import subprocess as subp

# Configure dhcp server for 3 Hosts (u1, u2, u3)

def check(lines):
    for line in lines:
        if line[:7] == "#123456":
            print "You already have run this script"
            return True
    return False


def writehosts(HOST_NAME, HOST_MAC, HOST_IP, File):
    File.write("host " + HOST_NAME + " {\n")
    File.write("\t hardware ethernet " + HOST_MAC +";\n")
    File.write("\t fixed-address " + HOST_IP + ";\n")
    File.write("}\n")


deffile = open("/etc/default/isc-dhcp-server", "a+")
if check(deffile.readlines()):
    exit(0)
deffile.write("\n")
deffile.write('INTERFACES="eth1"\n')
deffile.write('#123456\n')
deffile.close()


dhcpfile = open("/etc/dhcp/dhcpd.conf", "a+")
if check(dhcpfile.readlines()):
    exit(0)
dhcpfile.write("\n authoritative;\n")
HOST_MACs = ["08:00:27:00:62:71", "08:00:27:12:d5:76", "08:00:27:99:8e:6c"]
for i in range(3):
    writehosts("u" + str(i+1), HOST_MACs[i], "192.168.0." + str(i + 1) + "0",
    dhcpfile)
dhcpfile.write("subnet 192.168.0.0 netmask 255.255.255.0 {\n")
dhcpfile.write("\t range 192.168.0.1 192.168.0.255;\n")
dhcpfile.write("\t option domain-name-servers 192.168.0.1;\n")
dhcpfile.write('\t option domain-name "lab.local";\n')
dhcpfile.write("\t option routers 192.168.0.1;\n")
dhcpfile.write("\t option broadcast-address 192.168.0.255;\n")
dhcpfile.write("\t default-lease-time 21600;\n")
dhcpfile.write("\t max-lease-time 21600;\n}\n")
dhcpfile.write("#123456\n")
dhcpfile.close()   
