import subprocess

user = subprocess.check_output(["hostname", "-f"], universal_newlines=True)
useraddress = (str(user.split('\n')[0])).strip('u') + '0'

subprocess.call(["ip", "addr", "add", "192.168.0." + useraddress + "/24",
"dev", "eth0"])

subprocess.call(["ip", "link", "set", "eth0", "up"])

subprocess.call(["ip", "route", "add", "via", "192.168.0.1", "dev", "eth0"])
