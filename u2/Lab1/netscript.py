import os
import subprocess

f = open("/etc/network/interfaces", "w")

user = subprocess.check_output(["hostname", "-f"], universal_newlines=True)
useraddress = (str(user.split('\n')[0])).strip('u') + '0'

commands = ['auto lo', 'iface lo inet loopback', 'auto eth0', 'iface eth0 inet static',
'address 192.168.0.' + useraddress, 'netmask 255.255.255.0', 'gateway 192.168.0.1']
for conf in commands:
    f.write(conf + '\n')
f.close()

f2 = open("/etc/resolv.conf", "w")
f2.write('\n')
f2.write("nameserver 192.168.0.1\n")
f2.close()

#Try to make a comment
